import riak

def main():
    my_client = riak.RiakClient(protocol='pbc')
    # get bucket
    my_bucket = my_client.bucket('s16347')
    # create new
    value1 = {"index": "s16347", "subject": "NBD", "totalPoints": 0}
    key1 = my_bucket.new('k1', value1)
    key1.store()
    # get and write
    get_doc_1 = my_bucket.get('k1')
    print(get_doc_1.key, get_doc_1.data)
    # modify
    get_doc_1.data["totalPoints"] = 80
    get_doc_1.store()
    # get and write
    get_doc_2 = my_bucket.get('k1')
    print(get_doc_2.key, get_doc_2.data)
    # delete
    get_doc_2.delete()
    # try get and write
    get_doc_3 = my_bucket.get('k1')
    print(get_doc_3.key, get_doc_3.data)


if __name__ == '__main__':
    main()

