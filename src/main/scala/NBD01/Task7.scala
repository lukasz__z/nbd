package NBD01

object Task7 {
  val productsPrices = Map("milk" -> 5.5, "beer" -> 1.0, "whisky" -> 99.99)


  def main(args: Array[String]) {
    //Domyślne działanie
    println("--Default--")
    println(productsPrices.get("milk")) //Some
    println(productsPrices.get("water")) //None

    //Wykorzystanie getOrElse
    println("\n--getOrElse--")
    println(productsPrices.getOrElse("milk",5.5)) //Some
    println(productsPrices.getOrElse("water",1.2)) //None

    //Sprawdzenie
    println("\n--isEmpty--")
    println(productsPrices.get("milk").isEmpty) //Some = true
    println(productsPrices.get("water").isEmpty) //None = false

    //Inne przykład - zastosowanie w funkcji
    println("\n--Other example--")
    println(checkAvailabilityProduct(productsPrices.get("milk"))) //Some = true
    println(checkAvailabilityProduct(productsPrices.get("water"))) //None = false
  }

  def checkAvailabilityProduct(prod : Option[Double]) : String = prod match {
    case Some(prod) => "Your product is availability!"
    case None => "Your product is NOT availability"
  }


}