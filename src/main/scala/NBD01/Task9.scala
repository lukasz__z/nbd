package NBD01

object Task9 {
  val numberList = List(1,2,0,-1,-100,12312,0,123,33,0,99)


  def main(args: Array[String]) {
    println("Before: " + numberList)
    println("After: " + increaseAllElementOfListByOne(numberList))
  }

  def increaseAllElementOfListByOne(list: List[Int]): List[Int] = {
    list map(e => e+1)
  }

}