package NBD01

object Task4 {
  val week: List[String] = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela")


  def main(args: Array[String]) {
    println("a. " + function_a(week))
    println("b. " + function_b(week))
    println("c. " + function_c(week))


  }

  def function_a(days: List[String]): String = {
    days.foldLeft("")(_ + _ + ",")
  }

  def function_b(days: List[String]): String = {
    days.foldRight("")(_ + "," + _ )
  }

  def function_c(days: List[String]): String = {
    days.foldLeft("")((z,f) => {
     if(f.startsWith("P")){
       z  + f + ","
     }else{
       z
     }
    })
  }







}