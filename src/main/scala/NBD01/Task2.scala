package NBD01

import scala.annotation.tailrec

object Task2 {
  val week: List[String] = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela")


  def main(args: Array[String]) {
    println("a. " + funtion_a(week))
    println("b. " + funtion_b(week))

  }

  def funtion_a(days: List[String]): String = {
    if(days.isEmpty){
      ""
    }else{
     if(days.tail.isEmpty) {
       days.head + funtion_a(days.tail)
     }else {
       days.head + ", " + funtion_a(days.tail)
     }
    }
  }

  def funtion_b(days: List[String]): String = {
    if(days.isEmpty){
      ""
    }else{
      if(days.length == 1) {
        days.last + funtion_b(days.dropRight(1))
      }else {
        days.last + ", " + funtion_b(days.dropRight(1))
      }
    }
  }



}