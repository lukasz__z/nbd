package NBD01

object Task1 {
  val week: List[String] = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela")


  def main(args: Array[String]) {
    println("a. " + function1(week))
    println("b. " + function2(week))
    println("c. " + function3(week))
  }

  def function1(days: List[String]): String = {
    var string: String = days.head
    for (day <- days.tail) {
      string += ", " + day
    }
    string
  }

  def function2(days: List[String]): String = {
    var string: String = ""
    var first = true
    for (day <- days if day.startsWith("P")) {
      if (first) {
        string += day
        first = false
      } else {
        string += ", " + day
      }
    }
    string
  }

  def function3(days: List[String]): String = {
    var string: String = days.head
    var i = 0
    while (i < days.tail.length) {
      string += ", " + days(i + 1)
      i += 1
    }
    string
  }

}