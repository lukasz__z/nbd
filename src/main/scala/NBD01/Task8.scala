package NBD01

import scala.annotation.tailrec

object Task8 {
  val numbers: List[Int] = List(-1,0,10,0,100,99,-2,3,0,-23)


  def main(args: Array[String]) {
    println(deletesZeroesFromList(numbers, List()))
  }

  @tailrec
  def deletesZeroesFromList(numbers: List[Int],result : List[Int]) : List[Int] = numbers match{
    case Nil  => result.reverse
    case head :: tail => {
      if(head == 0){
        deletesZeroesFromList(tail,result)
      }else{
        deletesZeroesFromList(tail,head :: result)
      }
    }

  }




}
