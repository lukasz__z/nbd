package NBD01

object Task5 {
  var productsPrices = Map("milk" -> 5.5, "beer" -> 1.0, "whisky" -> 99.99)


  def main(args: Array[String]) {
    println("Before -10%: " + productsPrices)
    val productsPricesMinus10 = productsPrices map{case (k,v) => k -> v*0.9}
    println("After -10%: " + productsPricesMinus10)
  }

}