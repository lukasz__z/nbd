package NBD01

object Task6 {
  val exampleTuple = Tuple3("NBD",1100.00,2)


  def main(args: Array[String]) {
    writeTupleWith3Values(exampleTuple)
  }

  def writeTupleWith3Values(tuple : (String, Double, Int)){
    println("Value 1: " + tuple._1)
    println("Value 2: " + tuple._2)
    println("Value 3: " + tuple._3)
  }

}