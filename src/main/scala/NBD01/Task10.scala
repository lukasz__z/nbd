package NBD01
import scala.math.abs

object Task10 {

  def main(args: Array[String]) {
    val numberList = List(-20,-15,-5,-4,-3,0,5,10,15,20)
    println(increaseAllElementOfListByOne(numberList))

  }

  def increaseAllElementOfListByOne(list: List[Int]): List[Int] = {
    list.filter(e => e >= -5 && e <= 12).map(abs)
  }

}