printjson(
    db.people.aggregate(
        {$addFields: {
            weightDouble: { $convert: { input: "$weight", to: "double",  onNull: 0}},
            heightDouble: { $convert: { input: "$height", to: "double",  onNull: 0}},
        }},
        {
            $group: {
            _id: "$sex",
            avg_weight: {
            $avg : "$weightDouble",
            },
            avg_height: {
            $avg : "$heightDouble",
            }
        }}
    ).toArray()   
)