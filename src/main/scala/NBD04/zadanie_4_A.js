printjson(
    db.people.aggregate(
        {   $addFields: {
                weightDouble: { $convert: { input: "$weight", to: "double",  onNull: 0}},
                heightDouble: { $convert: { input: "$height", to: "double",  onNull: 0}},
            }
        },
        {
            $group: {
              _id: "$nationality",
              min_bmi : {$min : {$divide: ["$weightDouble", {$pow: [ {$divide : ["$heightDouble", 100] }, 2]}]}},
              max_bmi : {$max : {$divide: ["$weightDouble", {$pow: [ {$divide : ["$heightDouble", 100] }, 2]}]}},
              avg_bmi : {$avg : {$divide: ["$weightDouble", {$pow: [ {$divide : ["$heightDouble", 100] }, 2]}]}}
    
            }
        }
    ).toArray()   
)