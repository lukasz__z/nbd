printjson(
  db.people.mapReduce(
        function() {
            for(item of this.credit){
               emit(item.currency, {balanceFloat: parseFloat(item.balance)});
            }
        },
        function(key, values) {
          result = {totalBalance : 0};
          for (value of values) {
              result.totalBalance += value.balanceFloat;
          }
          return result;  
        },
        {
            out: {inline: 1},
            finalize: function(key, value) {
               return {
                  currency: key,
                  sum_balance: value.totalBalance
               }    
            }
        }
      )
)