printjson(
  db.people.mapReduce(
        function() {
           emit(this.nationality, {weightFloat: parseFloat(this.weight), heightFloat : parseFloat(this.height)});
        },
        function(key, values) {
          result = {count: 0, totalBMI: 0, minBMI: 100, maxBMI: 0};
          for (value of values) {
              result.count += 1;
              var bmi = value.weightFloat / ((value.heightFloat /100) * (value.heightFloat /100))
              result.totalBMI += bmi;
              if(bmi < result.minBMI){
                  result.minBMI = bmi;
              }
              if(bmi > result.maxBMI){
                  result.maxBMI = bmi;
              }
          }
          return result;  
        },
        {
            out: {inline: 1},
            finalize: function(key, value) {
               return {
                  nationality: key,
                  avg_BMI: value.totalBMI / value.count,
                  max_BMI: value.maxBMI,
                  min_BMI: value.minBMI 
               }    
            }
        }
      )

)