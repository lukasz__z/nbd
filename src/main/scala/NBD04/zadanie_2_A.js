printjson(
    db.people.aggregate(
        {
        $unwind: "$credit"
        },
        {
            $addFields: {
              balanceDouble: { $convert: { input: "$credit.balance", to: "double",  onNull: 0}}
            }
        },
        {
            $group: {
              _id: "$credit.currency",
              sum_balance: {
                $sum: "$balanceDouble"
              }
            }
        }
    ).toArray()  
)