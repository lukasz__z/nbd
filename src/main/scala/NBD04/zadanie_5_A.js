printjson(
    db.people.aggregate(
        {
            $unwind: "$credit"
        },
        {
            $addFields: {
              balanceDouble: { $convert: { input: "$credit.balance", to: "double",  onNull: 0}}
            }
        },
        {
            $match: {
              "sex" : "Female",
              "nationality" : "Poland"
            }      
        },
        {
            $group: {
              _id: "$credit.currency",
              sum_balance: {
                $sum: "$balanceDouble"
              },
              avg_balance: {
                $avg: "$balanceDouble"
              }
            }
        }
    ).toArray()   
)