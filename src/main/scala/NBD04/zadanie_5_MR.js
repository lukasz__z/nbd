printjson(
   db.people.mapReduce(
        function() {
            for(i of this.credit){
                emit(i.currency, {balanceFloat: parseFloat(i.balance), nationality : this.nationality, sex : this.sex});
            }
        },
        function(key, values) {
          result = {count: 0, totalBalance: 0};
          for (value of values) {
              if(value.nationality == "Poland" && value.sex == "Female"){
                  result.count += 1;
                  result.totalBalance += value.balanceFloat;
              }
          }
          return result;  
        },
        {
            out: {inline: 1},
            finalize: function(key, value) {
               return {
                  currency: key,
                  avg_balance: value.totalBalance / value.count,
                  total_balance: value.totalBalance
               }    
            }
        }
      )

)