printjson(
  db.people.mapReduce(
        function() {
           emit(this.sex, {weightFloat: parseFloat(this.weight), heightFloat : parseFloat(this.height)});
        },
        function(key, values) {
          result = {count: 0, totalWeight: 0,totalHeight: 0};
          for (value of values) {
              result.count += 1;
              result.totalWeight += value.weightFloat;
              result.totalHeight += value.heightFloat;
          }
          return result;  
        },
        {
            out: {inline: 1},
            finalize: function(key, value) {
               return {
                  sex: key,
                  avg_weight: value.totalWeight / value.count,
                  avg_height: value.totalHeight / value.count
               }    
            }
        }
      )
)