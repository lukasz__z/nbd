printjson(
    db.people.insertOne(
        {
        "sex": "Male",
        "first_name": "Lukasz",
        "last_name": "Zajkowski",
        "job": "COBOL developer",
        "email": "test@test.com",
        "location": {
            "city": "Warsaw",
            "address": {
            "streetname": "Marszałkowska",
            "streetnumber": "1"
            }
        },
        "description": "student PJATK",
        "height": "176",
        "weight": "80.99",
        "birth_date": "1996-08-22T07:22:24",
        "nationality": "Poland",
        "credit": [
            {
            "type": "mastercard",
            "number": "56022541652073648",
            "currency": "PLN",
            "balance": "1000000.01"
            },
        ]
        }
    )
)