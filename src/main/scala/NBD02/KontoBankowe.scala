package NBD02

class KontoBankowe(private var _stanKonta : Double) {

  def stanKonta : Double = _stanKonta

  def this() = this(0)

   def wplata(kwota : Double){
     _stanKonta = _stanKonta + kwota;
   }

  def wyplata(kwota : Double) : AnyVal ={
    if(stanKonta >= kwota){
      _stanKonta = _stanKonta - kwota
      _stanKonta
    }
  }

  override def toString = s"Stan konta: " + stanKonta
}
