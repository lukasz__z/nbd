package NBD02

object Task3 {
  val osoba1 = new Osoba("Jan","Kowalski")
  val osoba2 = new Osoba("Jan","Nowak")
  val osoba3 = new Osoba("Adam","Mickiewicz")
  val osoba4 = new Osoba("Lech","Wałęsa")
  val osoba5 = new Osoba("Andrzej","Duda")
  val osoba6 = new Osoba("Igancy","Kowal")
  val list = List(osoba1,osoba2,osoba3,osoba4,osoba5,osoba6)

  def main(args: Array[String]) {
      list.foreach(os => {
        println(greeting(os))
      })
  }

  def greeting(osoba: Osoba) : String = osoba.nazwisko match {
    case "Duda" | "Wałęsa" => "Witamy Pan Prezydenta " + osoba.nazwisko
    case "Kowalski" => "Cześć " + osoba.nazwisko
    case "Mickiewicz" => "Czołem " + osoba.nazwisko
    case _ => "Dzień dobry " + osoba.nazwisko


  }
}
