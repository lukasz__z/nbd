package NBD02

trait Pracownik extends Osoba2 {
  override def podatek: Double = 0.2*_pensja
  private var _pensja : Double= 0.0
  def pensja_=(newVal:Double) = _pensja = newVal
  def pensja = _pensja

}
