package NBD02

object Task5 {

  def main(args: Array[String]) {
      val student = new Osoba2("Jan","Kowalski") with Student
      val pracownik = new Osoba2("Jan","Kowalski") with Pracownik
      pracownik.pensja = 1000
      val nauczyciel = new Osoba2("Jan","Kowalski") with Nauczyciel
      nauczyciel.pensja = 1000

      println("Student.podatek: " + student.podatek)
      println("Pracownik.podatek: " + pracownik.podatek)
      println("Nauczyciel.podatek: " + nauczyciel.podatek)

    val studentPracownik = new Osoba2("Jan","Kowalski") with Student with Pracownik
    studentPracownik.pensja = 1000
    println("studentPracownik.podatek: " + studentPracownik.podatek) // Podatek Pracownika bo jest ostatni w kolejności

    val pracownikStudent = new Osoba2("Jan","Kowalski") with Pracownik with Student
    pracownikStudent.pensja = 1000
    println("pracownikStudent.podatek: " + pracownikStudent.podatek) //Podatek Studenta bo jest ostatni w kolejności

    val pracownikNauczyciel = new Osoba2("Jan","Kowalski") with Pracownik with Nauczyciel
    pracownikNauczyciel.pensja = 1000
    println("pracownikNauczyciel.podatek: " + pracownikNauczyciel.podatek) //Podatek Nauczyciela bo nadpisuje Pracownika

    val nauczycielPracownik = new Osoba2("Jan","Kowalski") with Nauczyciel with Pracownik
    nauczycielPracownik.pensja = 1000
    println("nauczycielPracownik.podatek: " + nauczycielPracownik.podatek) //Podatek Nauczyciela bo nadpisuje Pracownika

    val studentPracownikNauczyciel = new Osoba2("Jan","Kowalski") with Student with Pracownik with Nauczyciel
    studentPracownikNauczyciel.pensja = 1000
    println("studentPracownikNauczyciel.podatek: " + studentPracownikNauczyciel.podatek) //Podatek Nauczyciela bo nadpisuje Pracownika i jest po Studencie

    val pracownikNauczycielStudent = new Osoba2("Jan","Kowalski") with Pracownik with Nauczyciel with Student
    pracownikNauczycielStudent.pensja = 1000
    println("pracownikNauczycielStudent.podatek: " + pracownikNauczycielStudent.podatek) //Podatek Studenta bo jest ostatni


  }
}
