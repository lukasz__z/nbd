package NBD02

object Task1 {
  val week: List[String] = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela","Styczeń")

  def main(args: Array[String]) {
    week.foreach(s => {
      print("\n" + s + " = " + checkDay(s))
    })
  }

  def checkDay(day : String) : String = day match {
    case "Poniedziałek" | "Wtorek" | "Środa" | "Czwartek" | "Piątek" => "Praca"
    case "Sobota" | "Niedziela" => "Weekend"
    case _ => "Nie ma takiego dnia"
  }

}