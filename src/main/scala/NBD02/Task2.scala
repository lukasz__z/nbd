package NBD02

object Task2 {


  def main(args: Array[String]) {
    //Test klasy KontoBankowe
    val konto1 = new KontoBankowe()
    println(konto1.toString())
    konto1.wplata(1000)
    println(konto1.toString())
    konto1.wyplata(100)
    println(konto1.toString())

    var konto2 = new KontoBankowe(1000000)
    println(konto2.toString())
    konto1.wplata(1000000)
    println(konto2.toString())
    konto1.wyplata(0.01)
    println(konto2.toString())

  }

}
