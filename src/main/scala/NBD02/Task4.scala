package NBD02

import javax.security.auth.callback.Callback

object Task4 {

  def main(args: Array[String]) {
    println(executionFunction(10,addTen))
    println(executionFunction(2,squared))
  }

  def addTen(num : Int): Int ={
      num+10
  }

  def squared(num : Int): Int ={
    num*num
  }

  def executionFunction(number : Int,method:(Int) => Int): Int ={
    var result = number
    for(_ <- 1 to 3){
      result = method(result)
    }
    result
  }

}
